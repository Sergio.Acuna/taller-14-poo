function iniciar() {
    //crear una escena 
    //THREE
    let escena =new THREE.Scene();
    //crear camara
    let camara= new THREE.PerspectiveCamera(75,window.innerWidth / window.innerHeight, 1, 100);

    camara.position.z = 70;

    //crear lienzo
    let lienzo= new THREE.WebGLRenderer();
    lienzo.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( lienzo.domElement );
    
    //crear geometria
    let cubo= new THREE.SphereGeometry( 15, 32, 16 );
    let material= new THREE.MeshBasicMaterial( { color: 0x000FF, wireframe: true } );

    //Crear la malla de la escena 
    let miCubo= new THREE.Mesh( cubo, material );

    //agregar la malla 

    escena.add( miCubo );


    let m;
    let d;

    let animar = function(){
    let num =requestAnimationFrame(animar);  //real sea el objeto, requerimiento a la jecución de animación
    
    if(num<(window.innerWidth-1000)){
    miCubo.rotation.y= miCubo.rotation.y + 0.005;
    miCubo.rotation.x= miCubo.rotation.x + 0.005;
    miCubo.position.x= miCubo.position.x - 0.3;
    
     }else{
        m = miCubo.rotation.y= miCubo.rotation.y + 0.005;
        miCubo.rotation.x= miCubo.rotation.x + 0.005;
        miCubo.position.x= miCubo.position.x + 0.3;
        console.log(m)
     }

     if(m>=4.2699999999999525){
      miCubo.rotation.y= miCubo.rotation.y - 0.005;
      miCubo.rotation.x= miCubo.rotation.x - 0.005;
      miCubo.position.x= miCubo.position.x - 0.3;
      miCubo.position.y= miCubo.position.y - 0.3;
      miCubo.position.y= miCubo.position.y - 0.3;

    }
    if(num<(window.innerHeight-1000)){
      miCubo.rotation.y= miCubo.rotation.y + 0.005;
      miCubo.rotation.x= miCubo.rotation.x + 0.005;
      miCubo.position.y= miCubo.position.y + 0.3;
      
       }else{
          d = miCubo.rotation.x= miCubo.rotation.x + 0.005;
          miCubo.rotation.y= miCubo.rotation.y + 0.005;
          miCubo.position.y= miCubo.position.y + 0.3;

          console.log(d)
       }
  
       if(d<=1.0699999999999525){
        miCubo.rotation.x= miCubo.rotation.x - 0.005;
        miCubo.rotation.y= miCubo.rotation.y - 0.005;
        miCubo.position.y= miCubo.position.y - 0.3;
        miCubo.position.y= miCubo.position.y - 0.3;

        if(d<=-1.0699999999999525){
          miCubo.rotation.x= miCubo.rotation.x - 0.005;
          miCubo.rotation.y= miCubo.rotation.y - 0.005;
          miCubo.position.y= miCubo.position.y -  0.3;
  
        

        }
      }  
    lienzo.render(escena,camara);
    }
    animar();

}

iniciar();